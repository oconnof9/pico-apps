#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pico/stdlib.h"
#include "pico/float.h"     // Required for using single-precision variables.
#include "pico/double.h"    // Required for using double-precision variables.

void wallis_product_algorithm_double(double total_wallis);
void wallis_product_algorithm_float(float total_wallis);

/**
 * @brief LAB #02 - TEMPLATE
 *        Main entry point for the code.
 * 
 * @return int      Returns exit-status zero on completion.
 */

int main() {

#ifndef WOKWI
    
    /**
     * @brief   Initialise the IO as we will be using the UART
     *          Only required for hardware and not needed for Wokwi
     */

    stdio_init_all();
#endif

    float total_float = 1.0;
    double total_double = 1.0;
 
    /**
    * @brief Print a console message to inform user what's going on.
    */

    printf("Calculating Pi using Single Precision Floating-Point!\n");

    /**
    * @brief Call on the Wallis Product algorithm that uses 
    * single-precision (float) floating-point representation
    */    
    wallis_product_algorithm_float(total_float);

    /**
    * @brief Print a console message to inform user what's going on.
    */
    printf("Calculating Pi using Double Precision Floating-Point!\n");

    /**
    * @brief Call on the Wallis Product algorithm that uses 
    * double-precision (double) floating-point representation
    */ 
    wallis_product_algorithm_double(total_double);

    /**
    * @brief Returning zero indicates everything went okay.
    */ 
    return 0;
}


/**
 * @brief   this void funtion implements the wallis algorithm using 
 *          double-precision floating-point representation
 * 
 * @param total_wallis 
 */
void wallis_product_algorithm_double(double total_wallis){
    double unit_one, unit_two; 

    /**
     * @brief wallis product algorithm implemented here
     * 
     */

    for (double i = 1; i <= 100000; i++){
        unit_one = (2 * i) / ( (2 * i) - 1);
        unit_two = (2 * i) / ( (2 * i) + 1);

        total_wallis *= unit_one * unit_two;
    }

    /**
     * @brief as doubles have 15 digits of precision, I used
     * %.15f when printing both the value of Pi and the
     * approximation error
     */

    double calculated_PI2 = total_wallis * 2;

    printf("Double precision floating-point Pi = %.15f\n", calculated_PI2);

    double comp_pi2 = 3.14159265359; // maybe define globally

    /**
     * @brief approximation error calculated here and then printed to console
     * 
     */

    double approx_error2 = ( ( (calculated_PI2 - comp_pi2) / comp_pi2) );

    printf("Approximation Error Double = %.15f", approx_error2);

}  

/**
 * @brief   this void funtion implements the wallis algorithm using 
 *          single-precision floating-point representation
 * 
 * @param total_wallis 
 */
void wallis_product_algorithm_float(float total_wallis){
    float unit_one, unit_two; 

    /**
     * @brief wallis product algorithm implemented here
     * 
     */
    for (float i = 1; i <= 100000; i++){
        unit_one = (2 * i) / ( (2 * i) - 1);
        unit_two = (2 * i) / ( (2 * i) + 1);

        total_wallis *= unit_one * unit_two;
    }

    float calculated_PI = total_wallis * 2;

    /**
     * @brief as floats have 7 digits of precision, I used
     * %.7f when printing both the value of Pi and the
     * approximation error
     */

    printf("Single precision floating-point Pi= %.7f\n", calculated_PI);

    float comp_pi = 3.14159265359; // maybe define globally

    /**
     * @brief approximation error calculated here and then printed to console
     * 
     */

    float approximation_error = ( ( (calculated_PI - comp_pi) / comp_pi ) );

    printf("Approximation Error Float = %.7f\n", approximation_error);
}  