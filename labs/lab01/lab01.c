#include "pico/stdlib.h"
/**
 * @brief LAB #01 - TEMPLATE
 *        Main entry point for the code.
 * 
 * @return int      Returns exit-status zero on completion.
 */

void lightToggle(int pin, int delay);

int main() {
    /**
    * Specify the PIN number and sleep delay
    */
    const uint LED_PIN   =  25;
    const uint LED_DELAY = 500;

    /**
    * Setup the LED pin as an output.
    */
    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);

  
    /**
    * Do forever...
    */
    while (true) {
        
        /**
        * call on subroutine
        */
        lightToggle(LED_PIN, LED_DELAY);

    }

    /**
    * Returning zero indicates everything went okay.
    */
    return 0;
}


/**
* void funtion set up to move content 
* of original while loop to a subroutine
*/
void lightToggle(int pin, int delay){                                           

    /**
    * Toggle the LED on and then sleep for delay period
    */
    gpio_put(pin, 1);
    sleep_ms(delay);

    /**
    * Toggle the LED off and then sleep for delay period
    */
    gpio_put(pin, 0);
    sleep_ms(delay);

}
