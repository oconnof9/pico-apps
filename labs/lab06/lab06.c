#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pico/stdlib.h"
#include "pico/float.h"     // Required for using single-precision variables.
#include "pico/double.h"    // Required for using double-precision variables.
#include "pico/multicore.h" // Required for using multiple cores on the RP2040.

/**
 * @brief   This function acts as the main entry-point for core #1.
 *          A function pointer is passed in via the FIFO with one
 *          incoming int32_t used as a parameter. The function will
 *          provide an int32_t return value by pushing it back on
 *          the FIFO, which also indicates that the result is ready.
 *
 */
void core1_entry()
{
    while (1)
    {
        int32_t (*func)() = (int32_t(*)())multicore_fifo_pop_blocking();
        int32_t p = multicore_fifo_pop_blocking();
        int32_t result = (*func)(p);
        multicore_fifo_push_blocking(result);
    }
}

/**
 * @brief this void funtion implements the wallis algorithm using
 *          double-precision floating-point representation
 *
 * @param total_wallis
 * @return int32_t
 */
int32_t wallis_product_algorithm_double(int32_t total_wallis)
{
    double unit_one, unit_two;

    double result = 1.0;

    for (double i = 1; i <= total_wallis; i++)
    {
        unit_one = (2 * i) / ((2 * i) - 1);
        unit_two = (2 * i) / ((2 * i) + 1);

        result *= unit_one * unit_two;
    }

    double calculated_PI2 = result * 2;

    // printf("\nDouble precision floating-point Pi = %.15f\n\n", calculated_PI2);

    return calculated_PI2;
}

/**
 * @brief this void funtion implements the wallis algorithm using
 *          single-precision floating-point representation
 *
 * @param total_wallis
 * @return int32_t
 */
int32_t wallis_product_algorithm_float(int32_t total_wallis)
{
    float unit_one, unit_two;

    float result = 1.0;
    // printf("%f\n", result);

    for (float i = 1; i <= total_wallis; i++)
    {
        unit_one = (2 * i) / ((2 * i) - 1);
        unit_two = (2 * i) / ((2 * i) + 1);

        result *= unit_one * unit_two;
    }

    float calculated_PI = result * 2;

    // printf("\nSingle precision floating-point Pi = %.7f\n\n", calculated_PI);

    return calculated_PI;
}

/**
 * @brief LAB #02 - TEMPLATE
 *        Main entry point for the code.
 *
 * @return int      Returns exit-status zero on completion.
 */
int main()
{
    stdio_init_all();

    //
    // Sequentially
    //

    printf("\n\n-----\nHello, multicore_runner!\n");

    const int ITER_MAX = 100000;
    float res;
    double res2;

    // launch the second core
    multicore_launch_core1(core1_entry);

    // measure the current time for comparisons later
    absolute_time_t time_before = get_absolute_time();
    absolute_time_t total_time = time_before;

    // calls on function to be run on the second core
    multicore_fifo_push_blocking((uintptr_t)&wallis_product_algorithm_float);
    multicore_fifo_push_blocking(ITER_MAX);

    res = multicore_fifo_pop_blocking();

    // measure the current time for comparisons later
    absolute_time_t time_after = get_absolute_time();
    int64_t diff = absolute_time_diff_us(time_before, time_after);
    printf("Single-precision function execution time on single core = %lld microseconds\n", diff);

    // measure the current time for comparisons later
    time_before = get_absolute_time();

    // calls on second function to be run on the second core
    multicore_fifo_push_blocking((uintptr_t)&wallis_product_algorithm_double);
    multicore_fifo_push_blocking(ITER_MAX);

    res2 = multicore_fifo_pop_blocking();

    // measure the current time for comparisons later
    time_after = get_absolute_time();

    // OUTPUT THE DIFFERENCE IN TIMES
    absolute_time_t total_finish = time_after;
    diff = absolute_time_diff_us(time_before, time_after);
    printf("Double-precision function execution time on single core = %lld microseconds\n", diff);

    diff = absolute_time_diff_us(total_time, total_finish);
    printf("Total execution time runnung sequentially using a single CPU core = %lld microseconds\n", diff);

    //
    // Parallel now
    //

    time_before = get_absolute_time();
    absolute_time_t time_before2 = time_before;
    total_time = time_before;

    // call on one function to be run on the second core
    multicore_fifo_push_blocking((uintptr_t)&wallis_product_algorithm_float);
    multicore_fifo_push_blocking(ITER_MAX);

    // time_after = get_absolute_time();
    // diff = absolute_time_diff_us(time_before, time_after);
    // printf("Single-precision function execution time when using both cores = %lld microseconds\n", diff);

    // while the other function is running on the second core, let's run the code for the other function on this core
    time_before = get_absolute_time();

    double unit_one, unit_two;

    double result = 1.0;

    for (double i = 1; i <= ITER_MAX; i++)
    {
        unit_one = (2 * i) / ((2 * i) - 1);
        unit_two = (2 * i) / ((2 * i) + 1);

        result *= unit_one * unit_two;
    }

    double calculated_PI2 = result * 2;

    // printf("\nDouble precision floating-point Pi = %.15f\n\n", calculated_PI2);

    time_after = get_absolute_time();

    diff = absolute_time_diff_us(time_before, time_after);

    printf("Double-precision function execution time when using both cores = %lld microseconds\n", diff);

    // get the result of the other function on the other core
    res = multicore_fifo_pop_blocking();
    total_finish = get_absolute_time();

    // OUTPUT THE DIFFERENCE IN TIMES
    diff = absolute_time_diff_us(time_before2, total_finish);
    printf("Single-precision function execution time when using both cores = %lld microseconds\n", diff);

    diff = absolute_time_diff_us(total_time, total_finish);
    printf("Total execution time runnung in parallel across both CPU cores = %lld microseconds\n", diff);

    return 0;
}
